package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import accueil.Abonne;
import accueil.BanException;
import accueil.Bibliotheque;
import accueil.Document;
import accueil.NotExistException;
import accueil.PasLibreException;

public class Tests {

	private Bibliotheque bbl;
	private Document livre;
	private Abonne peter;
	private Abonne jack;

	@Before
	public void init() throws NotExistException, BanException{
		bbl = Bibliotheque.getInstance();
	}

	@Test
	public void testBan(){

		try {
			peter = bbl.getAbonne(1);
			peter.ban();
			livre = bbl.getDocument(1);
		} catch (NotExistException | BanException e1) {
			fail("Peter existe,le livre aussi et Peter n'est pas ban");
		}
		try {
			livre.emprunter(peter);
			fail();
		}catch(Exception e){

		}
	}

	@Test
	public void testResa(){

		try{
			peter = bbl.getAbonne(2);
			jack = bbl.getAbonne(3);
			livre = bbl.getDocument(2);
			livre.reserver(jack);
		}
		catch(Exception e){
			fail();
		}

		try {
			livre.emprunter(peter);
			fail();
		}catch(Exception e){

		}
	}

	@Test
	public void testEmprunt(){
		try{
			peter = bbl.getAbonne(4);
			jack = bbl.getAbonne(5);
			livre = bbl.getDocument(3);
			livre.emprunter(peter);
		}
		catch(Exception e){
			fail();
		}
		
		try {
			livre.emprunter(jack);
			fail("Jack ne doit pas pouvoir emprunter ce livre");
		}catch(PasLibreException e){
		}
	}
}
