package retour;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

import accueil.Abonne;
import accueil.BanException;
import accueil.Bibliotheque;
import accueil.NotExistException;
import accueil.PasLibreException;
import accueil.Service;

public class ServiceRetour implements Service{

	private Socket client;
	private static final int TEMPSPAUSE = 3 * 60 * 1000; // en ms
	
	private ServiceRetour(Socket socket) {
		client = socket;
	}

	public ServiceRetour() {
		// Pour cr�er le service initiale
	}

	public void run() {
		System.out.println("Nouveau client : " + client.getInetAddress());
		try {
			@SuppressWarnings("resource")
			Scanner in = new Scanner (client.getInputStream ( ));
			PrintWriter out = new PrintWriter (client.getOutputStream ( ), true);
			Bibliotheque bbl = Bibliotheque.getInstance();

			String reponse = "";
			while (true) {
				new StopTask(this, TEMPSPAUSE).run();
				out.println("Le num�ro de livre que vous souhaitez retourner :");
				

				int document = in.nextInt();
				String retour = "";
				out.println("Le livre est abim�?(y)");
				reponse = in.next();
				try {
				if (!reponse.equals("y"))
					bbl.retourner(document, true);
				else 
					bbl.retourner(document, false);
				}catch (NotExistException e) {
					retour = e.getMessage();
				}
				
				out.println(retour + " Voulez vous retourner un autre livre?(y)");
				reponse = in.next();
				if (!reponse.equals("y")){
					out.println("Ok bye!");
					client.close();
					in.close();
					System.err.println("Fin d'un service de retour ");
					return;
				}
			}
		}
		catch (IOException | NoSuchElementException e) {}
		System.err.println("Fin d'un service de retour ");
		try {client.close();} catch (IOException e2) {}
	}

	public void finalize() throws Throwable {
		client.close(); 
	}

	public void lancer() {
		if (client != null)
			(new Thread(this)).start();		
		else throw new RuntimeException();
	}

	@Override
	public Service getClone(Socket s) {
		return new ServiceRetour(s);
	}

}
