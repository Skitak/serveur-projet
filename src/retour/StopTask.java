package retour;

import java.util.TimerTask;

import accueil.Service;

public class StopTask extends TimerTask{

	private static int TEMPSPAUSE; // en ms
	private Service service;
	
	public StopTask(Service service, int tempsPause){
		this.service = service;
		this.TEMPSPAUSE = tempsPause;
	}
	@Override
	public void run() {
		try {
			Thread.sleep(TEMPSPAUSE);

			service.finalize();
		} catch (Throwable e) {	
		}
	}
}
