package retour;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class Application {

	private final static int PORT_RETOUR = 2700;
	private final static String ADR_IP_BIBLIO = "localhost";
	

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner clavier = new Scanner(System.in);
		
		Socket laSocket = null;
		Scanner socketIn = null;
		try {
			laSocket = new Socket(ADR_IP_BIBLIO, PORT_RETOUR);
			socketIn = new Scanner(laSocket.getInputStream());
			PrintWriter socketOut = new PrintWriter(laSocket.getOutputStream(), true);

			System.out.println("Bienvenue sur votre syst�me de retour : ");
			System.out.println("Vous pouvez ici retourner un livre");
			System.out.println(socketIn.nextLine());
			while (true){
				socketOut.println(clavier.nextLine());
				//resultat de la requ�te envoy� par le serveur.
				String reponse = "";
				if (socketIn.hasNext())
					reponse += socketIn.nextLine();
				System.out.println(reponse);
			}
			
		} catch (IOException | NoSuchElementException e) {
			System.out.println("Le serveur est ferm�, revenez plus tard.");
			try {
				laSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			socketIn.close();

		}
		try {
			laSocket.close();
		} catch (IOException e) {e.printStackTrace();}
	}

}