package reservation;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Application {

	private final static int PORT_RESERVATION = 2500;
	private final static String ADR_IP_BIBLIO = "localhost";

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner clavier = new Scanner(System.in);
		
		Socket laSocket = null;
		Scanner socketIn = null;
		try {
			laSocket = new Socket(ADR_IP_BIBLIO, PORT_RESERVATION);

			socketIn = new Scanner(laSocket.getInputStream());
			PrintWriter socketOut = new PrintWriter(laSocket.getOutputStream(), true);
			
			
			System.out.println("Bienvenue sur votre syst�me de r�servation : ");
			System.out.println("Vous pouvez ici r�server un livre disponible ");
			System.out.println("et passer le chercher dans les 2 heures.");
			System.out.println(socketIn.nextLine());
			while (true){
				socketOut.println(clavier.nextLine());
				//resultat de la requ�te envoy� par le serveur.
				String reponse = "";
				if (socketIn.hasNext())
					reponse += socketIn.nextLine();
				System.out.println(reponse);
			}
			
		} catch (IOException | NoSuchElementException e) {
			System.out.println("Le serveur est ferm�, revenez plus tard.");
			try {
				laSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			socketIn.close();
		}
		try {
			laSocket.close();
		} catch (IOException e) {e.printStackTrace();}
	}

}
