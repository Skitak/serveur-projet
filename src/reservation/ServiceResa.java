package reservation;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import accueil.Abonne;
import accueil.BanException;
import accueil.Bibliotheque;
import accueil.NotExistException;
import accueil.PasLibreException;
import accueil.Service;
import retour.StopTask;

public class ServiceResa implements Service{

	private static final int TEMPSPAUSE = 10 * 60 * 1000; // en ms
	private Socket client;
	private ServiceResa(Socket socket) {
		client = socket;
	}

	public ServiceResa() {
		//Fait pour construire le service Initiale
	}

	public void run() {
		System.out.println("Nouveau client : " + client.getInetAddress());
		try {
			Scanner in = new Scanner (client.getInputStream ( ));
			PrintWriter out = new PrintWriter (client.getOutputStream ( ), true);
			Bibliotheque bbl = Bibliotheque.getInstance();
			Abonne ab = null;
			out.println("Quel est votre numero d'abonn�?");
			while (ab == null){
				try {
					ab = bbl.getAbonne( in.nextInt());
				} catch (NotExistException |BanException e) {
					//L'abonn� n'existe pas.
					out.println(e.getMessage() + " Veuillez entrer un autre numero.");
				}
			}
			String reponse = "";

			while (true) {
				new StopTask(this, TEMPSPAUSE).run();
				out.println("Le num�ro de livre que vous souhaitez r�server :");
				//TODO verif input
				int document = in.nextInt();
				String retour = "";
				try {

					bbl.reserver(document, ab);
					retour = "Le livre " + document + " a �t� r�serv� � votre nom!";
				} catch (PasLibreException | NotExistException e) {
					retour = e.getMessage();
				}
				out.println(retour + " Voulez vous r�server un autre livre?(y)");
				reponse = in.next();
				if (!reponse.equals("y")){
					out.println("Ok bye!");
					client.close();
					in.close();
					System.err.println("Fin d'un service de r�servation ");
					return;
				}
			}
		}
		catch (IOException | NoSuchElementException e) {}
		System.err.println("Fin d'un service de r�servation ");
		try {client.close();} catch (IOException e2) {}
	}

	public void finalize() throws Throwable {
		client.close(); 
	}

	public void lancer() {
		if (client != null)
			(new Thread(this)).start();		
		else throw new RuntimeException();
	}

	@Override
	public Service getClone(Socket s) {
		return new ServiceResa(s);
	}

}
