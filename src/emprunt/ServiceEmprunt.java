package emprunt;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

import accueil.Abonne;
import accueil.BanException;
import accueil.Bibliotheque;
import accueil.NotExistException;
import accueil.PasLibreException;
import accueil.Service;
import retour.StopTask;

public class ServiceEmprunt implements Service{

	private static final int TEMPSPAUSE = 3 * 60 * 1000; // en ms
	private Socket client;

	private ServiceEmprunt(Socket socket) {
		client = socket;
	}

	public ServiceEmprunt(){
		//Service initiale
	}

	public void run() {
		System.out.println("Nouveau client : " + client.getInetAddress());
		try {
			@SuppressWarnings("resource")
			Scanner in = new Scanner (client.getInputStream ( ));
			PrintWriter out = new PrintWriter (client.getOutputStream ( ), true);
			Bibliotheque bbl = Bibliotheque.getInstance();
			Abonne ab = null;

			while (ab == null){
				try {
					ab = bbl.getAbonne( in.nextInt());
				} catch (NotExistException | BanException e) {
					//L'abonn� n'existe pas.
					System.out.println(e.getMessage());
				}
			}

			String reponse = "";

			while (true) {
				new StopTask(this, TEMPSPAUSE).run();
				out.println("Le num�ro de livre que vous souhaitez emprunter :");
				//TODO verif input
				int document = in.nextInt();
				String retour = "";
				try {

					bbl.emprunter(document, ab);
					retour = "Le livre " + document + " a �t� emprunt� � votre nom!";
				} catch (PasLibreException | NotExistException e) {
					retour = e.getMessage();
				}
				out.println(retour + " Voulez vous emprunter un autre livre?(y)");
				reponse = in.next();
				if (!reponse.equals("y")){
					out.println("Ok bye!");
					client.close();
					in.close();
					System.err.println("Fin d'un service d'emprunt ");
					return;
				}
			}
		}
		catch (IOException | NoSuchElementException e) {}
		System.err.println("Fin d'un service d'emprunt ");
		try {client.close();} catch (IOException e2) {}
	}

	public void finalize() throws Throwable {
		client.close(); 
	}

	public void lancer() {
		if (client != null)
			(new Thread(this)).start();		
		else throw new RuntimeException();
	}

	@Override
	public Service getClone(Socket s) {
		return new ServiceEmprunt(s);
	}

}
