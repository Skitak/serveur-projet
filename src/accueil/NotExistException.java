package accueil;

public class NotExistException extends Exception {
	public NotExistException(String info) {
		super(info);
	}
}
