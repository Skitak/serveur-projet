package accueil;

import java.net.Socket;

public interface Service extends Runnable{
	Service getClone (Socket s);

	void lancer();

	void finalize() throws Throwable;

}
