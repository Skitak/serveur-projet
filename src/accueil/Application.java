package accueil;

import java.io.IOException;
import emprunt.ServiceEmprunt;
import reservation.ServiceResa;
import retour.ServiceRetour;


public class Application {

	private final static int PORT_RESA = 2500;
	private final static int PORT_EMPRUNT = 2600;
	private final static int PORT_RETOUR = 2700;

 
	
	public static void main(String[] args) {
		
		Bibliotheque.getInstance();
		
		try {
			new Serveur(PORT_RESA, new ServiceResa()).lancer();
			new Serveur(PORT_RETOUR, new ServiceRetour()).lancer();
			new Serveur(PORT_EMPRUNT, new ServiceEmprunt()).lancer();
			System.out.println("Serveur d'emprunt lance sur les ports : \n-" + PORT_RESA + " : R�servation \n-" + PORT_EMPRUNT + " : Retour \n-" + PORT_RETOUR + " : Emprunt");

		} catch (IOException e) {
			System.err.println("Pb lors de la cr�ation du serveur : " +  e);			
		}

	}

}
