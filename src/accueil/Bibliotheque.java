package accueil;

import java.util.ArrayList;

public class Bibliotheque {

	private ArrayList<Abonne> abonnees = new ArrayList<>();
	private ArrayList<Document> documents = new ArrayList<>();
	
	private static Bibliotheque instance;
	
	private Bibliotheque () {
		loadData();
	}
	
	private void loadData() {
		for (int i = 0; i < 10000 ; ++i){
			abonnees.add(new Abonne(i));
			documents.add(new Livre(i));
		}
	}
	
	public static Bibliotheque getInstance(){
		if (instance == null)
			instance = new Bibliotheque();
		return instance;
	}

	public Abonne getAbonne(int i) throws NotExistException, BanException{
		for (Abonne ab : abonnees)
			if (ab.getNum() == i){
				if (ab.estBan())
					throw new BanException("Cet abonn� est banni.");
				return ab;
			}
		throw new NotExistException ("L'abonn�e " + i + " n'existe pas.");
	}
	
	public Document getDocument(int i) throws NotExistException {
		for (Document d : documents)
			if (d.numero() == i)
				return d;
		throw new NotExistException ("Le livre " + i + " n'existe pas.");
	}

	public void reserver(int document, Abonne abonne) throws PasLibreException, NotExistException {
		getDocument(document).reserver(abonne);
	}

	public void emprunter(int document, Abonne abonne) throws PasLibreException, NotExistException {
		getDocument(document).emprunter(abonne);
	}
	
	public void retourner(int noDocument, boolean abime) throws NotExistException{
		Document document = getDocument(noDocument);
		if (abime){
			Abonne ab = getAbonne(document);
			if (ab != null)
				ab.ban();
		}
		document.retour();
	}

	private Abonne getAbonne(Document document) {
		for (Abonne ab : abonnees)
			if (ab.possede(document))
				return ab;
		return null;
	}
}
