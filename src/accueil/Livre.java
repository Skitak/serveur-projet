package accueil;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Livre implements Document{

	private int numero;
	private Abonne reserveur;
	private Abonne emprunteur;
	private Calendar reservationTime;
	private Calendar empruntTime;


	private static final int TEMPS_RESA_H = 2; //Temps en heures
	private static final int  TEMPS_EMPRUNT_W = 3; //Temps en semaines
	private static final int TEMPS_EMPRUNT_RETARD_W = 2; //Temps en semaines

	public Livre (int numero){
		this.numero = numero;
		reserveur = null;
		emprunteur = null;
	}

	@Override
	public int numero() {
		return numero;
	}

	@Override
	public synchronized void reserver(Abonne ab) throws PasLibreException {
		if (reserveur != null){
			Calendar clone = (Calendar) reservationTime.clone();
			clone.add(Calendar.HOUR_OF_DAY, TEMPS_RESA_H);
			if (clone.after(new GregorianCalendar()))
				throw new PasLibreException("Ce livre est d�j� r�serv�!");
		}
		else if (emprunteur != null)
			throw new PasLibreException("Ce livre est d�j� emprunt�!");
		else if (ab.estBan())
			throw new PasLibreException("Cet utilisateur est ban");
		reserveur = ab;
		reservationTime = new GregorianCalendar();
	}

	@Override
	public synchronized void emprunter(Abonne ab) throws PasLibreException {
		if (emprunteur != null)
			throw new PasLibreException("Le livre est d�j� emprunt�!");
		else if (reserveur != null && ab != reserveur){
			Calendar clone = (Calendar) reservationTime.clone();
			clone.add(Calendar.HOUR_OF_DAY, 2);
			if (clone.after(new GregorianCalendar()))
				throw new PasLibreException("Ce livre est d�j� r�serv�!");
		}
		else if (ab.estBan())
			throw new PasLibreException("Cet utilisateur est banni pour le moment!");
		reserveur = null;
		reservationTime = null;
		emprunteur = ab;
		emprunteur.emprunter(this);
		empruntTime = new GregorianCalendar();
	}

	@Override
	public synchronized void retour(){
		if (emprunteur != null){
			emprunteur.retourner(this);
			empruntTime.add(Calendar.WEEK_OF_YEAR, TEMPS_EMPRUNT_W + TEMPS_EMPRUNT_RETARD_W);
			if (empruntTime.before(new GregorianCalendar()))
				emprunteur.ban();
			emprunteur = null;
			empruntTime = null;
		}
	}

	@Override
	public String toString (){
		return "Livre numero " + numero;
	}

}
