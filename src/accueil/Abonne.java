package accueil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Abonne {

	private int numeroAb;
	private GregorianCalendar ban;
	private ArrayList<Document> emprunts;
	
	private static final int BAN_TIME = 1; //Temps en mois

	public Abonne (int numero){
		numeroAb = numero;
		emprunts = new ArrayList<Document>();
	}

	public int getNum(){
		return numeroAb;
	}

	public String toString(){
		return "Abonn�e num�ro " + numeroAb ; 
	}

	public void ban() {
		ban = new GregorianCalendar();
	}

	public Calendar getBanDate(){
		return ban;
	}

	public boolean estBan() {

		if (ban != null){
			GregorianCalendar clone = (GregorianCalendar) ban.clone();
			clone.add(Calendar.MONTH, BAN_TIME);
			return clone.after(new GregorianCalendar());
		}
		return false;
	}
	
	public void emprunter (Document d) throws PasLibreException{
		emprunts.add(d);
	}
	
	public void retourner (Document d){
		emprunts.remove(d);
	}
	
	public boolean possede(Document d){
		return emprunts.contains(d);
	}

}
