package accueil;

import java.io.*;
import java.net.*;

import reservation.ServiceResa;


class Serveur implements Runnable {
	private ServerSocket listen_socket;
	private Service monService;

	Serveur(int port, Service  s) throws IOException {
		listen_socket = new ServerSocket(port);
		monService = s;
	}

	public void run() {
		try {
			while(true)
				monService.getClone(listen_socket.accept()).lancer();
		}
		catch (IOException e) { 
			try {this.listen_socket.close();} catch (IOException e1) {}
			System.err.println("Pb sur le port d'�coute :"+e);
		}
	}

	protected void finalize() throws Throwable {
		try {this.listen_socket.close();} catch (IOException e1) {}
	}

	public void lancer() {
		(new Thread(this)).start();
	}
}
